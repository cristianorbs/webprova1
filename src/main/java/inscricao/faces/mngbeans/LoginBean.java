/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inscricao.faces.mngbeans;

import inscricao.faces.model.User;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author cristianopc
 */
@ManagedBean(eager=true)
@ApplicationScoped
public class LoginBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String name;
    private String password;
    public boolean admin;   
    private List users = new ArrayList(); 

    public List getUsers() {
        return users;
    }

    public void setUsers(List users) {
        this.users = users;
    }
    
    
    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
       
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
      
    public void entrar(){
        if(!validateInput()){
            name = "";
            password = "";
            admin = false;
            FacesMessage message = new FacesMessage("Acesso negado");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Denied Accesss", message);
        }
        else{            
            try{
            if(admin){
                User user = new User();
                user.setName(name);
                user.setTime(new Date());
                users.add(user);
                name = "";
                password = "";
                admin = false;
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect(context.getRequestContextPath() + "/admin.xhtml");
            }
            else{
                User user = new User();
                user.setName(name);
                user.setTime(new Date());
                users.add(user);
                name = "";
                password = "";
                admin = false;
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect(context.getRequestContextPath() + "/cadastro.xhtml");
            }
            }catch(IOException e){
                name = "";
                password = "";
                admin = false;
                e.printStackTrace();
            }
        }
    }  
    
    private boolean validateInput(){
        if(name.equals(password)){
            return true;
        }
        return false;
    }
}
